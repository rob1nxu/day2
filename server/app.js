console.log("starting...");
var express = require("express");
var app = express();
console.log(__dirname);
app.use(express.static(__dirname + "/../client/")); /* this is to tell express where are the static files, register and initialise the endpoints*/
const NODE_PORT = process.env.NODE_PORT || 3000; /* the 3000 port no. is a fallback in case the node port is not defined */

app.listen (NODE_PORT, function() {

    console.log("Web App started at " + NODE_PORT);

});

/* tells ppl when they get to the wrong page*/
app.use(function(req, res) {
  console.log("app use");
    res.send("sorry wrong door"); // this can also be in html
  
}); 

app.get("/users", function(req, res) {
    console.log('users');
    var users = [ {
        name: 'robin',
        age: 30 
    },

    { 
        name: "chan",
        age: 40
    }];

res.json(users); //this json function will pass the data into a json database

}); // app.get is express middleware

function hi(name) {
    console.log("Hello ! -- " + name);
}

function hi2(name) {
    console.log("Hello2 ! -- " + name);
}

function sayHi(callback, callback2, name) {
    console.log("Inside sayHi");
    callback(name);
    callback2(name);
}

// the callback is to keep functions modular so that we can reuse them later. as compared to nested functions, functions within functions they can only be used once. 

sayHi(hi, hi2, "Kenneth Phang");
